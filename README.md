# makedeb

![](https://img.shields.io/badge/written%20in-Golang-blue)

A library for producing deb packages (Debian/Ubuntu) on other platforms.

- Confirmed working with current versions of `dpkg`
- Includes a sample application that almost certainly doesn't do /exactly/ what you wanted, but should be customisable for your needs
- Only minor warnings on `lintian` (depending on your content)

## Installation

Library: `go get -u code.ivysaur.me/makedeb`

Sample application: `go get -u code.ivysaur.me/makedeb/cmd/makedeb`

## Changelog

2016-10-01: r3
- Initial public release
- [⬇️ makedeb-r3.zip](dist-archive/makedeb-r3.zip) *(8.31 KiB)*

